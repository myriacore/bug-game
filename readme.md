# Bug Game

Bug Game is a WIP tamagotchi-like game where you nurture a pet bug and watch it
grow. 

I'm using this project to learn fundamentals of game development. After the end
of this project, I'd like to have learned:

- Model modification in blender
- 3D Game Development in Godot
  * How to create a good character & camera controller in 3d
  * How to create UI and do scene transitions
  * etc
- vfx stuff (particle effects & shaders)
- Rigging & 3D animation in blender
- Material creation in blender

Hopefully things work out nicely!

## Credits

All bug models by [Google](https://poly.google.com/user/4aEd8rQgKu2) via CC-BY.

## Resources / Videos Used

- [This timelapse video of a dude rigging an ant](https://youtu.be/JewlCPMmSjI)
  really helped me get started doing the smart way. I'm glad I found his video.
- [This basic tutorial for how to use
  IK](https://www.youtube.com/watch?v=S-2v_CKmVE8) helped a lot too, at least
  for understanding what's happening when setting up IK
- [This video of a scorpion walkin around](https://youtu.be/GjMI4u8wVBU) was
  useful to understand how scorpions walk so I could animate their walking
  cycle nicely
